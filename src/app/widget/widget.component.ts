import {Component, Input, OnInit} from '@angular/core';
import {WeatherItem} from '../main/main.component';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit {
  @Input('data') data: WeatherItem;

  constructor() {
  }

  ngOnInit() {
    console.log(this.data);
  }

}
