import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(
    private http: HttpClient
  ) {
  }

  getWeather() {
    return this.http.get('http://api.openweathermap.org/data/2.5/weather?q=Kiev&units=metric&APPID=2022be5eb20c409aa52caa2c656902e6');
  }
}
