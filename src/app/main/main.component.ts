import {AfterContentInit, Component, OnInit} from '@angular/core';
import {WeatherService} from '../weather.service';

import {fromEvent, interval, merge} from 'rxjs';
import {switchMapTo} from 'rxjs/operators';


export interface WeatherItem {
  city: string;
  icon: string;
  description: string;
  main: string;
  clouds: number;
}

export interface Data {
  coord: {};
  weather: [
    {
      id: number;
      main: string;
      description: string;
      icon: string;
    }
    ],
  base: string;
  main: {};
  wind: {};
  clouds: {
    all: number;
  };
  rain: {};
  dt: number;
  sys: {};
  id: number;
  name: string;
  cod: number;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterContentInit {
  displayedColumns: string[] = ['city', 'icon', 'description', 'main', 'clouds'];
  weatherSource: WeatherItem[] = [];
  weatherLastItem;
  refreshBtn;

  constructor(
    private weatherService: WeatherService,
  ) {
  }

  ngOnInit() {


  }

  ngAfterContentInit() {
    this.refreshBtn = document.querySelector('#refresh');
    const Interval$ = interval(20000);
    const Weather$ = this.weatherService.getWeather();
    const RefreshBtn$ = fromEvent(this.refreshBtn, 'click');
    console.log(this.refreshBtn);
    merge(Interval$, RefreshBtn$)
      .pipe(
        switchMapTo(
          Weather$
        )
      )
      .subscribe(
        (data: Data) => {
          const item: WeatherItem = {
            city: data.name,
            icon: data.weather[0].icon,
            description: data.weather[0].description,
            main: data.weather[0].main,
            clouds: data.clouds.all,
          };
          this.weatherSource.push(item);
          if (this.weatherSource.length > 20) {
            this.weatherSource.splice(1, 1);
          }
          this.weatherLastItem = item;
        }
      );
  }
}
